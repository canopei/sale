package main

import (
	"io/ioutil"

	"github.com/Sirupsen/logrus"
	"github.com/jmoiron/sqlx"
)

// NewServerMock creates a mock of Server
func NewServerMock(db *sqlx.DB) *Server {
	logger := logrus.WithFields(logrus.Fields{})
	// Mute the logger
	logger.Logger.Out = ioutil.Discard

	return NewServer(logger, db)
}
