package main

import (
	"golang.org/x/net/context"

	"github.com/Sirupsen/logrus"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/jmoiron/sqlx"
)

// Server is the account service implementation
type Server struct {
	Logger *logrus.Entry
	DB     *sqlx.DB
}

// NewServer creates a new Server instance
func NewServer(
	logger *logrus.Entry,
	db *sqlx.DB,
) *Server {
	return &Server{
		Logger: logger,
		DB:     db,
	}
}

// Ping is used for healthchecks
func (s *Server) Ping(ctx context.Context, req *empty.Empty) (*empty.Empty, error) {
	return &empty.Empty{}, nil
}
