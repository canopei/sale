package main

import (
	"database/sql"
	"strings"
	"time"

	"golang.org/x/net/context"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"

	"bitbucket.org/canopei/golibs/crypto"
	grpcUtils "bitbucket.org/canopei/golibs/grpc/utils"
	salePb "bitbucket.org/canopei/sale/protobuf"
)

// CreatePurchase creates a new Purchase
func (s *Server) CreatePurchase(ctx context.Context, req *salePb.Purchase) (*salePb.Purchase, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("CreatePurchase: %v", req)
	defer timeTrack(logger, time.Now(), "CreatePurchase")

	// Validations
	if req.AccountId < 1 {
		logger.Errorf("The account ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The account ID is required.")
	}
	req.Products = strings.TrimSpace(req.Products)
	if req.Products == "" {
		logger.Error("The products are required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The products are required.")
	}

	// Create a UUID
	uuid, err := crypto.NewUUID()
	if err != nil {
		logger.Errorf("Cannot generate a UUID: %v", err)
		return nil, grpcUtils.InternalError(logger, err, "Cannot generate a UUID")
	}
	req.Uuid = uuid.String()

	_, err = s.DB.NamedExec(`INSERT INTO purchase (uuid, account_id, reference_id, first_name, last_name, country, address,
		address2, city, state, zip, total, tax, products)
		VALUES (unhex(replace(:uuid,'-','')), :account_id, :reference_id, :first_name, :last_name, :country, :address,
		:address2, :city, :state, :zip, :total, :tax, :products)`, req)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot create database entry.")
	}

	var purchase = &salePb.Purchase{}
	err = s.DB.Get(purchase, "SELECT * FROM purchase WHERE uuid = UNHEX(REPLACE(?,'-',''))", req.Uuid)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("Cannot find the new purchase: %v", err)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		logger.Errorf("Error while fetching the new purchase: %v", err)
		return nil, err
	}
	purchase.Uuid = req.Uuid

	logger.Debugf("Created new purchase %d.", purchase.Id)

	return purchase, nil
}
