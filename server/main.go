package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"os"
	"strings"

	"bitbucket.org/canopei/golibs/auth"
	"bitbucket.org/canopei/golibs/healthcheck"
	"bitbucket.org/canopei/golibs/logging"
	"bitbucket.org/canopei/sale/config"
	salePb "bitbucket.org/canopei/sale/protobuf"
	"github.com/Sirupsen/logrus"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"

	"google.golang.org/grpc"
)

var (
	conf    *config.Config
	logger  *logrus.Entry
	db      *sqlx.DB
	version string
)

func main() {
	var err error

	configFile := flag.String("config", "config.toml", "the path to the config file")
	flag.Parse()

	envConfigFile := os.Getenv("SALE_CONFIG_FILE")
	if envConfigFile != "" {
		configFile = &envConfigFile
	}

	if conf, err = config.LoadConfig(*configFile); err != nil {
		logrus.WithFields(nil).Fatalf("Unable to read the config file: %v", err)
	}

	logger = logging.GetLogstashLogger(conf.Service.Env, conf.Service.Name, &conf.Logstash, logrus.Fields{
		"subservice": "grpc",
	})

	// Read the version from the disk
	b, err := ioutil.ReadFile("VERSION")
	if err != nil {
		logger.Fatalf("Cannot read the version file: %v", err)
	}
	version = strings.TrimSpace(string(b))

	logger.Infof("Booting '%s' server (%s)...", conf.Service.Name, version)

	// open a database connection
	dsn := fmt.Sprintf(
		"%s:%s@tcp(%s)/%s",
		conf.Db.Username,
		conf.Db.Password,
		conf.Db.Addr,
		conf.Db.DbName,
	)
	logger.Infof("Connecting to SQL - DSN: %s", dsn)
	db, err = sqlx.Connect("mysql", fmt.Sprintf(
		"%s:%s@tcp(%s)/%s",
		conf.Db.Username,
		conf.Db.Password,
		conf.Db.Addr,
		conf.Db.DbName,
	))
	if err != nil {
		logger.Fatalf("Unable to connect to the database: %v", err)
	}
	defer db.Close()

	// start the gRPC server
	s := NewServer(logger, db)
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", conf.Service.GrpcPort))
	if err != nil {
		logger.Fatalf("Failed to listen on tcp:%d: %v", conf.Service.GrpcPort, err)
	}

	opts := []grpc.ServerOption{
		grpc.UnaryInterceptor(auth.NewInterceptor(logger, conf.Auth.JWTSigningKey).UnaryServerInterceptor()),
	}
	grpcServer := grpc.NewServer(opts...)
	salePb.RegisterSaleServiceServer(grpcServer, s)

	// Setup a health check listener
	go func() {
		logger.Infof("Booting '%s' health check on %d...", conf.Service.Name, conf.Service.HealthPort)
		http.HandleFunc(healthcheck.Healthpath, healthcheck.Handler)
		http.ListenAndServe(fmt.Sprintf(":%d", conf.Service.HealthPort), nil)
	}()

	logger.Infof("Serving on %d.", conf.Service.GrpcPort)

	grpcServer.Serve(lis)
}
