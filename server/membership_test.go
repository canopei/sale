package main

import (
	"context"
	"testing"

	salePb "bitbucket.org/canopei/sale/protobuf"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	sqlmock "gopkg.in/DATA-DOG/go-sqlmock.v1"
)

var (
	membershipColumns = []string{"membership_id", "uuid", "name", "description", "count",
		"valid_for_days", "price", "tax", "is_featured", "is_available", "created_at", "modified_at", "deleted_at"}
)

func TestCreateMembershipValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *salePb.Membership
		ExpectedCode codes.Code
	}{
		{&salePb.Membership{}, codes.InvalidArgument},
		{&salePb.Membership{Description: "desc", Tax: 0.23, Count: 0, ValidForDays: 12, Price: 12.23}, codes.InvalidArgument},
		{&salePb.Membership{Name: "foo", Description: "desc", Tax: 0.23, Count: 0, ValidForDays: 12}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&salePb.Membership{Name: "foo", Price: 12.23}, codes.Unknown},
		{&salePb.Membership{Name: "foo", Description: "desc", Tax: 0.23, Count: 0, ValidForDays: 12, Price: 12.23}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		classDesc, err := server.CreateMembership(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(classDesc, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestCreateMembershipSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectExec("^INSERT INTO membership").WillReturnResult(sqlmock.NewResult(1, 1))
	rows := sqlmock.NewRows(membershipColumns).
		AddRow(1, "1234567890123456", "name", "description", 5, 30, 10.24, 0.16, true, true, "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM membership").WillReturnRows(rows)

	membership, err := server.CreateMembership(context.Background(), &salePb.Membership{
		Name: "name", Description: "desc", Tax: 0.23, Count: 0, ValidForDays: 12, Price: 12.23,
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(membership)
	if membership != nil {
		assert.NotEmpty(membership.Uuid)
		assert.Equal("name", membership.Name)
	}
}

func TestGetMembershipsSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	memberships := []*salePb.Membership{
		&salePb.Membership{Id: 1, Name: "foo 1", Description: "desc", Tax: 0.23, Count: 0, ValidForDays: 12, Price: 12.23},
		&salePb.Membership{Id: 2, Name: "foo 2", Description: "desc", Tax: 0.23, Count: 0, ValidForDays: 12, Price: 13.23},
		&salePb.Membership{Id: 3, Name: "foo 3", Description: "desc", Tax: 0.23, Count: 0, ValidForDays: 12, Price: 12.23},
	}
	membershipsRows := sqlmock.NewRows(membershipColumns)
	for _, memb := range memberships {
		membershipsRows.AddRow(memb.Id, "1234567890123456", memb.Name, memb.Description, memb.Count, memb.ValidForDays,
			memb.Price, memb.Tax, memb.IsFeatured, memb.IsAvailable, "2017-01-01", "2017-01-02", "2017-01-03")
	}
	mock.ExpectQuery("^SELECT (.+) FROM membership").WillReturnRows(membershipsRows)

	response, err := server.GetMemberships(context.Background(), &salePb.GetMembershipsRequest{Uuids: "uuid1,uuid2"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.Memberships, len(memberships))
		// Sanity check
		assert.Equal(float32(13.23), response.Memberships[1].Price)
	}
}

func TestGetMembershipsEmpty(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM membership").WillReturnRows(sqlmock.NewRows(membershipColumns))

	response, err := server.GetMemberships(context.Background(), &salePb.GetMembershipsRequest{Uuids: "1234567890123456,1234567890123455"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.Memberships, 0)
	}
}

func TestUpdateMembershipValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *salePb.Membership
		ExpectedCode codes.Code
	}{
		{&salePb.Membership{}, codes.InvalidArgument},
		{&salePb.Membership{Name: "foo", Price: 12, IsAvailable: false}, codes.InvalidArgument},
		{&salePb.Membership{Uuid: "", Name: "foo", Price: 12, IsAvailable: false}, codes.InvalidArgument},
		{&salePb.Membership{Uuid: "uuid", Price: 12, IsAvailable: false}, codes.InvalidArgument},
		{&salePb.Membership{Uuid: "uuid", Name: "foo", IsAvailable: false}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&salePb.Membership{Uuid: "uuid", Name: "foo", Price: 12, IsAvailable: false}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		classDesc, err := server.UpdateMembership(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(classDesc, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestUpdateMembershipMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectExec("^UPDATE membership SET").WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectQuery("^SELECT (.+) FROM membership WHERE").WillReturnRows(sqlmock.NewRows(membershipColumns))

	membership, err := server.UpdateMembership(context.Background(), &salePb.Membership{Uuid: "uuid", Name: "foo", Price: 12, IsAvailable: false})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(membership)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestUpdateMembershipSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	testMembership := &salePb.Membership{
		Uuid:         "1234567890123456",
		Name:         "foo",
		Description:  "desc",
		Tax:          0.23,
		Count:        0,
		ValidForDays: 12,
		Price:        12.23,
	}

	mock.ExpectExec("^UPDATE membership SET").WillReturnResult(sqlmock.NewResult(0, 1))
	rows := sqlmock.NewRows(membershipColumns).AddRow(1, "1234567890123456", "foo", "description", 5, 30, 10.24, 0.16, true, true, "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM membership WHERE").WillReturnRows(rows)

	membership, err := server.UpdateMembership(context.Background(), testMembership)
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(membership)
	// sanity check
	if membership != nil {
		assert.Equal("foo", membership.Name)
	}
}

func TestDeleteMembershipValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *salePb.Membership
		ExpectedCode codes.Code
	}{
		{&salePb.Membership{}, codes.InvalidArgument},
		{&salePb.Membership{Uuid: " "}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&salePb.Membership{Uuid: "uuid"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		response, err := server.DeleteMembership(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(response, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestDeleteMembership(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Not found error
	mock.ExpectExec("^UPDATE membership SET(.+)deleted_at").WillReturnResult(sqlmock.NewResult(0, 0))

	_, err = server.DeleteMembership(context.Background(), &salePb.Membership{Uuid: "uuid"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))

	// Successful delete
	mock.ExpectExec("^UPDATE membership SET(.+)deleted_at").WillReturnResult(sqlmock.NewResult(0, 1))
	_, err = server.DeleteMembership(context.Background(), &salePb.Membership{Uuid: "uuid"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
}
