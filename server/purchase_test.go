package main

import (
	"context"
	"testing"

	salePb "bitbucket.org/canopei/sale/protobuf"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	sqlmock "gopkg.in/DATA-DOG/go-sqlmock.v1"
)

var (
	purchaseColumns = []string{"purchase_id", "uuid", "account_id", "reference_id", "first_name", "last_name", "country", "address",
		"address2", "city", "state", "zip", "total", "tax", "products", "created_at", "modified_at"}
)

func TestCreatePurchaseValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *salePb.Purchase
		ExpectedCode codes.Code
	}{
		{&salePb.Purchase{}, codes.InvalidArgument},
		{&salePb.Purchase{AccountId: 2}, codes.InvalidArgument},
		{&salePb.Purchase{Products: "foo"}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&salePb.Purchase{AccountId: 2, Products: "foo"}, codes.Unknown},
		{&salePb.Purchase{AccountId: 2, Products: "foo", ReferenceId: "foo", FirstName: "foo", Address: "test", Total: 12.23, Tax: 0}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		purchase, err := server.CreatePurchase(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(purchase, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestCreatePurchaseSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectExec("^INSERT INTO purchase").WillReturnResult(sqlmock.NewResult(1, 1))
	rows := sqlmock.NewRows(purchaseColumns).
		AddRow(1, "1234567890123456", 2, "reference ID", "fname", "lname", "RO", "address", "", "city", "state", "zip", 20.23, 0, "[]", "2017-01-01", "2017-01-02")
	mock.ExpectQuery("^SELECT (.+) FROM purchase").WillReturnRows(rows)

	purchase, err := server.CreatePurchase(context.Background(), &salePb.Purchase{AccountId: 2,
		Products: "foo", FirstName: "foo", Address: "test", Total: 12.23, Tax: 0})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(purchase)
	if purchase != nil {
		assert.NotEmpty(purchase.Uuid)
		assert.Equal("RO", purchase.Country)
	}
}
