package main

import (
	"database/sql"
	"fmt"
	"strconv"
	"strings"
	"time"

	"golang.org/x/net/context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"

	"bitbucket.org/canopei/golibs/crypto"
	grpcUtils "bitbucket.org/canopei/golibs/grpc/utils"
	salePb "bitbucket.org/canopei/sale/protobuf"
)

// CreateMembership creates a new Membership
func (s *Server) CreateMembership(ctx context.Context, req *salePb.Membership) (*salePb.Membership, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("CreateMembership: %v", req)
	defer timeTrack(logger, time.Now(), "CreateMembership")

	// Validations
	req.Name = strings.TrimSpace(req.Name)
	if req.Name == "" {
		logger.Errorf("The membership name is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The membership name is required.")
	}
	if req.Price < 1 {
		logger.Error("The price is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The price is required.")
	}

	// Create a UUID
	uuid, err := crypto.NewUUID()
	if err != nil {
		logger.Errorf("Cannot generate a UUID: %v", err)
		return nil, grpcUtils.InternalError(logger, err, "Cannot generate a UUID")
	}
	req.Uuid = uuid.String()

	_, err = s.DB.NamedExec(`INSERT INTO membership (uuid, name, description, count, valid_for_days, price, tax, is_featured)
		VALUES (unhex(replace(:uuid,'-','')), :name, :description, :count, :valid_for_days, :price, :tax, :is_featured)`, req)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot create database entry.")
	}

	var membership = &salePb.Membership{}
	err = s.DB.Get(membership, "SELECT * FROM membership WHERE uuid = UNHEX(REPLACE(?,'-',''))", req.Uuid)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("Cannot find the new membership: %v", err)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		logger.Errorf("Error while fetching the new membership: %v", err)
		return nil, err
	}
	membership.Uuid = req.Uuid

	logger.Debugf("Created new membership %d.", membership.Id)

	return membership, nil
}

// GetMemberships fetches a list of memberships
func (s *Server) GetMemberships(ctx context.Context, req *salePb.GetMembershipsRequest) (*salePb.MembershipList, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetMemberships: %v", req)
	defer timeTrack(logger, time.Now(), "GetMemberships")

	req.Uuids = strings.TrimSpace(req.Uuids)
	reqUUIDs := map[string]bool{}
	if req.Uuids != "" {
		uuids := strings.Split(req.Uuids, ",")
		for _, uuid := range uuids {
			uuid := strings.TrimSpace(uuid)
			if uuid != "" {
				reqUUIDs[uuid] = true
			}
		}
	}

	logger.Debugf("Fetching memberships (UUIDs: %s)", req.Uuids)

	queryParams := map[string]interface{}{}

	condition := ""
	if len(reqUUIDs) > 0 {
		condition = " AND uuid IN ("
		idx := 0
		for v := range reqUUIDs {
			paramKey := "id" + strconv.Itoa(idx)
			condition = fmt.Sprintf("%sUNHEX(REPLACE(:%s,'-','')), ", condition, paramKey)
			queryParams[paramKey] = v
			idx++
		}
		condition = strings.TrimSuffix(condition, ", ") + ") "
	}

	rows, err := s.DB.NamedQuery(`SELECT * FROM membership WHERE deleted_at = '0'`+condition, queryParams)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the memberships")
	}

	membershipsList := &salePb.MembershipList{}
	for rows.Next() {
		membership := salePb.Membership{}
		err := rows.StructScan(&membership)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning a membership")
		}

		unpackedUUID, err := crypto.Parse([]byte(membership.Uuid))
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID for class description ID %d", membership.Id)
		}
		membership.Uuid = unpackedUUID.String()

		membershipsList.Memberships = append(membershipsList.Memberships, &membership)
	}

	return membershipsList, nil
}

// UpdateMembership updates a Membership
func (s *Server) UpdateMembership(ctx context.Context, req *salePb.Membership) (*salePb.Membership, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("UpdateMembership: %v", req)
	defer timeTrack(logger, time.Now(), "UpdateMembership")

	// Validations
	req.Uuid = strings.TrimSpace(req.Uuid)
	if req.Uuid == "" {
		logger.Error("The membership UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The membership UUID is required.")
	}
	req.Name = strings.TrimSpace(req.Name)
	if req.Name == "" {
		logger.Errorf("The membership name is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The membership name is required.")
	}
	if req.Price < 1 {
		logger.Error("The price is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The price is required.")
	}

	_, err := s.DB.NamedExec(`UPDATE membership SET
			name = :name, description = :description, count = :count, price = :price, valid_for_days = :valid_for_days, tax = :tax,
			is_featured = :is_featured, :is_available = is_available
		WHERE deleted_at = '0' AND uuid = UNHEX(REPLACE(:uuid,'-',''))`, req)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - update.")
	}

	var membership salePb.Membership
	err = s.DB.Get(&membership, "SELECT * FROM membership WHERE uuid = UNHEX(REPLACE(?,'-',''))", req.Uuid)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("UpdateMembership - membership not found: %d", req.Id)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - select.")
	}
	membership.Uuid = req.Uuid

	return &membership, nil
}

// DeleteMembership removes a Membership (sets deleted_at)
func (s *Server) DeleteMembership(ctx context.Context, req *salePb.Membership) (*empty.Empty, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("DeleteMembership: %v", req)
	defer timeTrack(logger, time.Now(), "DeleteMembership")

	req.Uuid = strings.TrimSpace(req.Uuid)
	if req.Uuid == "" {
		logger.Error("The membership UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The membership UUID is required.")
	}

	result, err := s.DB.Exec("UPDATE membership SET deleted_at = CURRENT_TIMESTAMP WHERE deleted_at = '0' AND uuid = UNHEX(REPLACE(?,'-',''))", req.Uuid)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database.")
	}
	if no, _ := result.RowsAffected(); no == 0 {
		logger.Errorf("DeleteMembership - membership not found: %d", req.Id)
		return nil, grpc.Errorf(codes.NotFound, "")
	}

	return &empty.Empty{}, nil
}
