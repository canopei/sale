package config

import (
	"bitbucket.org/canopei/golibs/auth"
	"bitbucket.org/canopei/golibs/logging"
	"github.com/BurntSushi/toml"
)

// ServiceConfig holds the service general configuration
type ServiceConfig struct {
	Name       string
	Env        string
	ApiPort    int16 `toml:"api_port"`
	GrpcPort   int16 `toml:"grpc_port"`
	HealthPort int16 `toml:"health_port"`
}

// DatabaseConfig holds the database specific configuration
type DatabaseConfig struct {
	Addr     string
	Username string
	Password string
	DbName   string `toml:"dbname"`
}

// MindbodyConfig holds the Mindbody configuration
type MindbodyConfig struct {
	SourceName     string `toml:"source_name"`
	SourcePassword string `toml:"source_password"`
}

// ClassServiceConfig holds the configuration for the Class service
type ClassServiceConfig struct {
	Addr string
}

// SiteServiceConfig holds the configuration for the Site service
type SiteServiceConfig struct {
	Addr string
}

// AccountServiceConfig holds the configuration for the Account service
type AccountServiceConfig struct {
	Addr string
}

// Config holds the entire configuration
type Config struct {
	Service        ServiceConfig          `toml:"service"`
	Db             DatabaseConfig         `toml:"database"`
	Logstash       logging.LogstashConfig `toml:"logstash"`
	Mindbody       MindbodyConfig         `toml:"mindbody"`
	ClassService   ClassServiceConfig     `toml:"class_service"`
	SiteService    SiteServiceConfig      `toml:"site_service"`
	AccountService AccountServiceConfig   `toml:"account_service"`
	Auth           auth.Config            `toml:"auth"`
}

// LoadConfig loads the configuration from the given filepath
func LoadConfig(filePath string) (*Config, error) {
	var conf Config
	_, err := toml.DecodeFile(filePath, &conf)

	return &conf, err
}
