package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"sort"
	"strconv"
	"strings"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"

	"github.com/Sirupsen/logrus"
	"github.com/gorilla/mux"

	accountPb "bitbucket.org/canopei/account/protobuf"
	classPb "bitbucket.org/canopei/class/protobuf"
	"bitbucket.org/canopei/mindbody"
	mbHelpers "bitbucket.org/canopei/mindbody/helpers"
	saleMb "bitbucket.org/canopei/mindbody/services/sale"
	"bitbucket.org/canopei/sale/config"
	salePb "bitbucket.org/canopei/sale/protobuf"
	sitePb "bitbucket.org/canopei/site/protobuf"
)

// SaleServer is the implementation of a Sale server
type SaleServer struct {
	Logger         *logrus.Entry
	ClassService   classPb.ClassServiceClient
	SiteService    sitePb.SiteServiceClient
	ClientService  accountPb.ClientServiceClient
	AccountService accountPb.AccountServiceClient
	SaleService    salePb.SaleServiceClient
	MindbodyConfig *config.MindbodyConfig

	SaleMBClient *saleMb.Sale_x0020_ServiceSoap
}

// Service holds a MB Service information
type Service struct {
	MBID        int32   `json:"mbId"`
	SiteUUID    string  `json:"siteUuid,omitempty"`
	Name        string  `json:"name"`
	Price       float64 `json:"price"`
	TaxIncluded float64 `json:"taxIncluded"`
	TaxRate     float64 `json:"taxRate"`
	Count       int32   `json:"count"`
}

// GetServicesResponse is a HTTP response
type GetServicesResponse struct {
	Services []*Service `json:"services"`
}

// ByPrice implements sort.Interface for []*Service based on the Price field.
type ByPrice []*Service

func (a ByPrice) Len() int           { return len(a) }
func (a ByPrice) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByPrice) Less(i, j int) bool { return a[i].Price < a[j].Price }

// SaleItem represents an item in a Sale
type SaleItem struct {
	MBID     int32   `json:"mbId"`
	Quantity int32   `json:"quantity"`
	Price    float64 `json:"price"`
}

// CreateSaleRequest represents a HTTP request
type CreateSaleRequest struct {
	AccountUUID string      `json:"accountUuid"`
	Items       []*SaleItem `json:"items"`
	ClassUUID   string      `json:"classUuid"`
}

// CreateSaleResponse represents a HTTP response
type CreateSaleResponse struct {
	SaleMBID      string      `json:"mbID"`
	AccountUUID   string      `json:"accountUuid"`
	Items         []*SaleItem `json:"items"`
	ClassUUID     string      `json:"classUuid"`
	SubTotal      float64     `json:"subTotal"`
	DiscountTotal float64     `json:"discountTotal"`
	TaxTotal      float64     `json:"taxTotal"`
	TotalPrice    float64     `json:"totalPrice"`
}

// NewSaleServer creates an instance of an SaleServer
func NewSaleServer(
	logger *logrus.Entry,
	classService classPb.ClassServiceClient,
	siteService sitePb.SiteServiceClient,
	clientService accountPb.ClientServiceClient,
	accountService accountPb.AccountServiceClient,
	saleService salePb.SaleServiceClient,
	mindbodyConfig *config.MindbodyConfig,
) *SaleServer {
	return &SaleServer{
		Logger:         logger,
		ClassService:   classService,
		SiteService:    siteService,
		ClientService:  clientService,
		AccountService: accountService,
		SaleService:    saleService,
		MindbodyConfig: mindbodyConfig,

		SaleMBClient: saleMb.NewSale_x0020_ServiceSoap("", false, nil),
	}
}

// HandleGetServicesForSite handles a HTTP request for listing available services to be bought from
// the given site
func (s *SaleServer) HandleGetServicesForSite(res http.ResponseWriter, req *http.Request) {
	logger := GetRequestLogger(req, s.Logger)

	vars := mux.Vars(req)
	siteUUID := vars["site_uuid"]

	logger.Infof("HandleGetServicesForSite for site '%s'", siteUUID)

	ctx := context.Background()

	// Fetch the site from DB
	dbSite, err := s.SiteService.GetSite(ctx, &sitePb.GetSiteRequest{Uuid: siteUUID})
	if err != nil {
		errorCode := grpc.Code(err)
		if errorCode == codes.NotFound {
			logger.Errorf("Site UUID %s not found in DB.", siteUUID)
			res.WriteHeader(http.StatusNotFound)
		} else {
			logger.Errorf("Error while fetching the site from DB.")
			res.WriteHeader(http.StatusInternalServerError)
		}
		return
	}

	services, err := s.GetServices(dbSite.MbId, 0)
	if err != nil {
		logger.Errorf("Mindbody API minor response error: %s", err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	for _, service := range services {
		service.SiteUUID = dbSite.Uuid
	}

	logger.Debugf("Found %d services in Mindbody for site UUID %s.", len(services), siteUUID)

	response := &GetServicesResponse{Services: services}

	sort.Sort(ByPrice(response.Services))

	s.writeJSONResponse(res, response)
	res.WriteHeader(http.StatusOK)
}

// HandleGetServicesForClass handles a HTTP request for listing available services to be bought for access
// to the given class.
func (s *SaleServer) HandleGetServicesForClass(res http.ResponseWriter, req *http.Request) {
	logger := GetRequestLogger(req, s.Logger)

	vars := mux.Vars(req)
	classUUID := vars["class_uuid"]

	logger.Infof("HandleGetServicesForClass for class '%s'", classUUID)

	ctx := context.Background()

	// Fetch the class from DB
	dbClassesResponse, err := s.ClassService.GetClasses(ctx, &classPb.GetClassesRequest{Uuids: classUUID})
	if err != nil {
		logger.Errorf("Error while fetching the class from DB.")
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	if len(dbClassesResponse.Classes) == 0 {
		logger.Errorf("Class UUID not found in DB.")
		res.WriteHeader(http.StatusNotFound)
		return
	}
	dbClass := dbClassesResponse.Classes[0]

	// Fetch the site from DB
	dbSite, err := s.SiteService.GetSiteByLocationID(ctx, &sitePb.GetSiteByLocationIDRequest{LocationId: dbClass.LocationId})
	if err != nil {
		errorCode := grpc.Code(err)
		if errorCode == codes.NotFound {
			logger.Errorf("Site UUID not found in DB.")
			res.WriteHeader(http.StatusNotFound)
		} else {
			logger.Errorf("Error while fetching the site from DB.")
			res.WriteHeader(http.StatusInternalServerError)
		}
		return
	}

	services, err := s.GetServices(dbSite.MbId, dbClass.MbId)
	if err != nil {
		logger.Errorf("Mindbody API minor response error: %s", err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	for _, service := range services {
		service.SiteUUID = dbSite.Uuid
	}

	logger.Debugf("Found %d services in Mindbody.", len(services))

	response := &GetServicesResponse{Services: services}

	sort.Sort(ByPrice(response.Services))

	s.writeJSONResponse(res, response)
	res.WriteHeader(http.StatusOK)
}

// HandleCreateSale handles a HTTP request for creating a new successful sale.
func (s *SaleServer) HandleCreateSale(res http.ResponseWriter, req *http.Request) {
	logger := GetRequestLogger(req, s.Logger)

	var request CreateSaleRequest
	decoder := json.NewDecoder(req.Body)
	err := decoder.Decode(&request)
	defer req.Body.Close()
	if err != nil {
		logger.Errorf("Cannot parse the request body JSON: %v", err)
		res.WriteHeader(http.StatusBadRequest)
		return
	}

	// Validations
	if request.AccountUUID == "" {
		logger.Error("Empty account UUID provided.")
		res.WriteHeader(http.StatusBadRequest)
		return
	}
	if len(request.Items) == 0 {
		logger.Error("No sale items provided.")
		res.WriteHeader(http.StatusBadRequest)
		return
	}

	vars := mux.Vars(req)
	siteUUID := vars["site_uuid"]

	logger.Infof("HandleCreateSale for site '%s': %#v", siteUUID, request)

	ctx := context.Background()

	// Fetch the site from DB
	dbSite, err := s.SiteService.GetSite(ctx, &sitePb.GetSiteRequest{Uuid: siteUUID})
	if err != nil {
		errorCode := grpc.Code(err)
		if errorCode == codes.NotFound {
			logger.Errorf("Site UUID not found in DB.")
			res.WriteHeader(http.StatusNotFound)
		} else {
			logger.Errorf("Error while fetching the site from DB.")
			res.WriteHeader(http.StatusInternalServerError)
		}
		return
	}

	// Fetch the account clients
	dbAccountClients, err := s.ClientService.GetAccountsClients(ctx, &accountPb.GetAccountsClientsRequest{AccountUuids: request.AccountUUID})
	if err != nil {
		logger.Errorf("Error while fetching the clients from DB.")
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	if len(dbAccountClients.AccountClients) == 0 {
		logger.Errorf("Account UUID '%s' has no clients.", request.AccountUUID)
		res.WriteHeader(http.StatusBadRequest)
		return
	}
	dbClients := dbAccountClients.AccountClients[request.AccountUUID]

	// Check if the account has a client on this site
	var client *accountPb.Client
	for _, dbClient := range dbClients.Clients {
		if dbClient.SiteId == dbSite.Id {
			client = dbClient
		}
	}
	if client == nil {
		logger.Errorf("Account UUID '%s' has no client for site ID %d.", request.AccountUUID, dbSite.Id)
		res.WriteHeader(http.StatusNotFound)
		return
	}

	// Fetch all the services to calculate the total amount
	services, err := s.GetServices(dbSite.MbId, 0)
	if err != nil {
		logger.Errorf("Mindbody API minor response error: %s", err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	servicesMap := map[int32]*Service{}
	for _, service := range services {
		servicesMap[service.MBID] = service
	}
	cartServices := []*Service{}
	for _, cartItem := range request.Items {
		service, ok := servicesMap[cartItem.MBID]
		if !ok {
			logger.Errorf("Cart item MB ID %d not found in the list of services for site MB ID %d.", cartItem.MBID, dbSite.MbId)
			res.WriteHeader(http.StatusBadRequest)
			return
		}
		cartServices = append(cartServices, service)
	}

	// Fetch the class if one was provided
	var dbClass *classPb.Class
	if request.ClassUUID != "" {
		dbClassesResponse, err := s.ClassService.GetClasses(ctx, &classPb.GetClassesRequest{Uuids: request.ClassUUID})
		if err != nil {
			logger.Errorf("Error while fetching the class from DB.")
			res.WriteHeader(http.StatusInternalServerError)
			return
		}
		if len(dbClassesResponse.Classes) == 0 {
			logger.Errorf("Class UUID not found in DB.")
			res.WriteHeader(http.StatusNotFound)
			return
		}
		dbClass = dbClassesResponse.Classes[0]
	}

	totalAmount := float64(0)
	mbItems := &saleMb.ArrayOfCartItem{}
	for _, item := range request.Items {
		service, ok := servicesMap[item.MBID]
		if !ok {
			logger.Errorf("Cart item MB ID %d not found in the list of services for site MB ID %d.", item.MBID, dbSite.MbId)
			res.WriteHeader(http.StatusBadRequest)
			return
		}

		item.Price = service.Price * float64(item.Quantity) * (1 + service.TaxRate)

		totalAmount += service.Price * float64(item.Quantity) * (1 + service.TaxRate)

		cartItem := &saleMb.CartItem{
			Item: &saleMb.Item{
				MBObject: &saleMb.MBObject{
					XSIType: "Service",
				},
				ID: strconv.FormatInt(int64(item.MBID), 10),
			},
			Quantity: &mindbody.CustomInt32{Int: item.Quantity},
		}

		if dbClass != nil {
			cartItem.ClassIDs = &saleMb.ArrayOfInt{Int: []*mindbody.CustomInt32{&mindbody.CustomInt32{Int: dbClass.MbId}}}
		}

		mbItems.CartItem = append(mbItems.CartItem, cartItem)
	}

	credsUsername := "_" + s.MindbodyConfig.SourceName
	credsPassword := s.MindbodyConfig.SourcePassword
	// This is an exception ONLY for the Sandbox Site, where we cannot authorize our master API creds
	if dbSite.MbId == -99 {
		credsUsername = "Siteowner"
		credsPassword = "apitest1234"
	}

	// Get the available services from MB
	mbRequest := mbHelpers.CreateSaleMBRequest([]int32{dbSite.MbId}, s.MindbodyConfig.SourceName, s.MindbodyConfig.SourcePassword)
	mbRequest.UserCredentials = &saleMb.UserCredentials{Username: credsUsername, Password: credsPassword, SiteIDs: mbRequest.SourceCredentials.SiteIDs}
	// Fetch the services for class from Mindbody
	mbCheckoutResponse, err := s.SaleMBClient.CheckoutShoppingCart(&saleMb.CheckoutShoppingCart{Request: &saleMb.CheckoutShoppingCartRequest{
		MBRequest: mbRequest,
		ClientID:  strconv.FormatInt(client.MbId, 10),
		CartItems: mbItems,
		Payments: &saleMb.ArrayOfPaymentInfo{
			PaymentInfo: []*saleMb.PaymentInfo{
				&saleMb.PaymentInfo{
					MBObject: &saleMb.MBObject{
						XSIType: "CustomPaymentInfo",
					},
					Amount: &mindbody.CustomFloat64{Float: totalAmount},
					ID:     &mindbody.CustomInt32{Int: dbSite.SngPaymentMethodMbId},
				},
			},
		},
	}})
	if err != nil {
		logger.Errorf("Mindbody API minor response error: %#v", err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	mbCheckoutResult := mbCheckoutResponse.CheckoutShoppingCartResult
	if mbCheckoutResult.ErrorCode.Int != 200 {
		logger.Errorf("Mindbody API minor response error: %s", mbCheckoutResult.Message)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	response := &CreateSaleResponse{
		SaleMBID:      mbCheckoutResult.ShoppingCart.ID,
		AccountUUID:   request.AccountUUID,
		ClassUUID:     request.ClassUUID,
		Items:         request.Items,
		SubTotal:      mbCheckoutResult.ShoppingCart.SubTotal.Float,
		DiscountTotal: mbCheckoutResult.ShoppingCart.DiscountTotal.Float,
		TaxTotal:      mbCheckoutResult.ShoppingCart.TaxTotal.Float,
		TotalPrice:    mbCheckoutResult.ShoppingCart.GrandTotal.Float,
	}

	s.writeJSONResponse(res, response)
	res.WriteHeader(http.StatusOK)
}

// CreatePurchaseRequest represents a HTTP request
type CreatePurchaseRequest struct {
	AccountUUID string  `json:"accountUuid"`
	ReferenceID string  `json:"referenceId"`
	FirstName   string  `json:"firstName"`
	LastName    string  `json:"lastName"`
	Country     string  `json:"country"`
	Address     string  `json:"address"`
	Address2    string  `json:"address2"`
	City        string  `json:"city"`
	State       string  `json:"state"`
	Zip         string  `json:"zip"`
	Products    string  `json:"products"`
	Total       float32 `json:"total"`
	Tax         float32 `json:"tax"`
}

// HandleCreatePurchase handles a HTTP request for creating a new purchase.
func (s *SaleServer) HandleCreatePurchase(res http.ResponseWriter, req *http.Request) {
	logger := GetRequestLogger(req, s.Logger)

	ctx := context.Background()

	var request CreatePurchaseRequest
	decoder := json.NewDecoder(req.Body)
	err := decoder.Decode(&request)
	defer req.Body.Close()
	if err != nil {
		logger.Errorf("Cannot parse the request body JSON: %v", err)
		res.WriteHeader(http.StatusBadRequest)
		return
	}

	// Validations
	if request.AccountUUID == "" {
		logger.Error("Empty account UUID provided.")
		res.WriteHeader(http.StatusBadRequest)
		return
	}
	request.Products = strings.TrimSpace(request.Products)
	if len(request.Products) == 0 {
		logger.Error("No products provided.")
		res.WriteHeader(http.StatusBadRequest)
		return
	}

	account, err := s.AccountService.GetAccount(ctx, &accountPb.GetAccountRequest{Search: request.AccountUUID})
	if err != nil {
		if grpc.Code(err) == codes.NotFound {
			logger.Error("The account was not found.")
			res.WriteHeader(http.StatusNotFound)
		} else {
			logger.Errorf("Error while fetching the account: %v", err)
			res.WriteHeader(http.StatusInternalServerError)
		}

		return
	}

	purchase, err := s.SaleService.CreatePurchase(ctx, &salePb.Purchase{
		AccountId:   account.Id,
		ReferenceId: request.ReferenceID,
		FirstName:   request.FirstName,
		LastName:    request.LastName,
		Country:     request.Country,
		Address:     request.Address,
		Address2:    request.Address2,
		City:        request.City,
		State:       request.State,
		Zip:         request.Zip,
		Products:    request.Products,
		Total:       request.Total,
		Tax:         request.Tax,
	})
	if err != nil {
		logger.Errorf("Error while creating the purchase: %v", err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	s.writeJSONResponse(res, purchase)
	res.WriteHeader(http.StatusOK)
}

// GetServices retrieves the avaliable services from MB (all, or only for a given class ID)
// TODO: CACHE THIS!
func (s *SaleServer) GetServices(siteMBID int32, classMBID int32) ([]*Service, error) {
	// Get the available services from MB

	credsUsername := "_" + s.MindbodyConfig.SourceName
	credsPassword := s.MindbodyConfig.SourcePassword

	mbRequest := mbHelpers.CreateSaleMBRequest([]int32{siteMBID}, s.MindbodyConfig.SourceName, s.MindbodyConfig.SourcePassword)
	mbRequest.PageSize = &mindbody.CustomInt32{Int: 100}
	mbRequest.UserCredentials = &saleMb.UserCredentials{Username: credsUsername, Password: credsPassword, SiteIDs: mbRequest.SourceCredentials.SiteIDs}

	getServicesRequest := &saleMb.GetServicesRequest{
		MBRequest: mbRequest,
	}
	if classMBID > 0 {
		getServicesRequest.ClassID = &mindbody.CustomInt32{Int: classMBID}
	}

	// Fetch the services for class from Mindbody
	mbServicesResponse, err := s.SaleMBClient.GetServices(&saleMb.GetServices{Request: getServicesRequest})
	if err != nil {
		return nil, fmt.Errorf("Mindbody API minor response error: %#v", err)
	}
	mbServicesResult := mbServicesResponse.GetServicesResult
	if mbServicesResult.ErrorCode.Int != 200 {
		return nil, fmt.Errorf("Mindbody API minor response error: %s", mbServicesResult.Message)
	}

	result := []*Service{}
	for _, mbService := range mbServicesResult.Services.Service {
		mbID, err := strconv.ParseInt(mbService.ID, 10, 32)
		if err != nil {
			logger.Errorf("Failed to parse service ID to int32: %#v", err)
			return nil, fmt.Errorf("Failed to parse service ID to int32: %#v", err)
		}

		taxIncluded := float64(0)
		if mbService.TaxIncluded != nil {
			taxIncluded = mbService.TaxIncluded.Float
		}

		result = append(result, &Service{
			MBID:        int32(mbID),
			Name:        mbService.Name,
			Price:       mbService.Price.Float,
			TaxIncluded: taxIncluded,
			TaxRate:     mbService.TaxRate.Float,
			Count:       mbService.Count.Int,
		})
	}

	return result, nil
}

// RegisterRoutes registers the routes to a mux router
func (s *SaleServer) RegisterRoutes(r *mux.Router) {
	r.HandleFunc("/v1/classes/{class_uuid}/services", s.HandleGetServicesForClass).Methods("GET")
	r.HandleFunc("/v1/sites/{site_uuid}/services", s.HandleGetServicesForSite).Methods("GET")
	r.HandleFunc("/v1/sites/{site_uuid}/sales", s.HandleCreateSale).Methods("POST")
	r.HandleFunc("/v1/purchases", s.HandleCreatePurchase).Methods("POST")
}

func (s *SaleServer) writeJSONResponse(res http.ResponseWriter, response interface{}) error {
	jsonResponse, err := json.Marshal(response)
	if err != nil {
		s.Logger.Error("Failed to Marshal the response.")
		res.WriteHeader(http.StatusInternalServerError)
		return err
	}

	res.Write([]byte(jsonResponse))
	return nil
}
