package main

import (
	"context"
	"encoding/json"
	"flag"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"

	"google.golang.org/grpc"

	"bitbucket.org/canopei/account"
	"bitbucket.org/canopei/class"
	cApi "bitbucket.org/canopei/golibs/api"
	cGrpc "bitbucket.org/canopei/golibs/grpc/utils"
	"bitbucket.org/canopei/golibs/healthcheck"
	cHttp "bitbucket.org/canopei/golibs/http"
	"bitbucket.org/canopei/sale"
	"bitbucket.org/canopei/sale/config"
	"bitbucket.org/canopei/site"
	"github.com/gorilla/mux"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"

	"fmt"

	accountPb "bitbucket.org/canopei/account/protobuf"
	classPb "bitbucket.org/canopei/class/protobuf"
	"bitbucket.org/canopei/golibs/logging"
	salePb "bitbucket.org/canopei/sale/protobuf"
	sitePb "bitbucket.org/canopei/site/protobuf"
	"github.com/Sirupsen/logrus"
	"github.com/gorilla/handlers"
	"github.com/urfave/negroni"
)

var (
	conf           *config.Config
	logger         *logrus.Entry
	classService   classPb.ClassServiceClient
	siteService    sitePb.SiteServiceClient
	clientService  accountPb.ClientServiceClient
	accountService accountPb.AccountServiceClient
	saleService    salePb.SaleServiceClient
	version        string
)

func run() error {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	mux := mux.NewRouter()
	mux.HandleFunc(healthcheck.Healthpath, healthcheck.Handler)

	saleServer := NewSaleServer(logger, classService, siteService, clientService, accountService, saleService, &conf.Mindbody)
	saleServer.RegisterRoutes(mux)

	gwmux := runtime.NewServeMux(runtime.WithMarshalerOption(runtime.MIMEWildcard, &runtime.JSONBuiltin{}))
	opts := []grpc.DialOption{grpc.WithInsecure(), grpc.WithBackoffMaxDelay(8 * time.Second)}

	err := salePb.RegisterSaleServiceHandlerFromEndpoint(ctx, gwmux, fmt.Sprintf("127.0.0.1:%d", conf.Service.GrpcPort), opts)

	if err != nil {
		return err
	}

	mux.PathPrefix("/").Handler(cGrpc.RestGatewayResponseInterceptor(gwmux))

	n := negroni.New()
	n.Use(cHttp.NewXRequestIDMiddleware(16))
	n.Use(cApi.NewServiceAPIMiddleware(logger))
	n.Use(cApi.NewCombinedLoggingMiddleware(logger, mux, handlers.CombinedLoggingHandler))
	n.Use(negroni.NewRecovery())

	logger.Infof("Serving on %d.", conf.Service.ApiPort)
	return http.ListenAndServe(fmt.Sprintf(":%d", conf.Service.ApiPort), n)
}

func main() {
	var err error

	configFile := flag.String("config", "config.toml", "the path to the config file")
	flag.Parse()

	envConfigFile := os.Getenv("SALE_CONFIG_FILE")
	if envConfigFile != "" {
		configFile = &envConfigFile
	}

	if conf, err = config.LoadConfig(*configFile); err != nil {
		logrus.WithFields(nil).Fatalf("Unable to read the config file: %v", err)
	}

	logger = logging.GetLogstashLogger(conf.Service.Env, conf.Service.Name, &conf.Logstash, logrus.Fields{
		"subservice": "api",
	})

	// Read the version from the disk
	b, err := ioutil.ReadFile("VERSION")
	if err != nil {
		logger.Fatalf("Cannot read the version file: %v", err)
	}
	version = strings.TrimSpace(string(b))

	logger.Infof("Booting '%s' API (%s)...", conf.Service.Name, version)

	classService, _, err = class.NewClient(conf.ClassService.Addr)
	if err != nil {
		logger.Fatalf("Cannot start the class service: %v", err)
	}

	siteService, _, err = site.NewClient(conf.SiteService.Addr)
	if err != nil {
		logger.Fatalf("Cannot start the site service: %v", err)
	}

	clientService, _, err = account.NewClientClient(conf.AccountService.Addr)
	if err != nil {
		logger.Fatalf("Cannot start the client service: %v", err)
	}

	accountService, _, err = account.NewAccountClient(conf.AccountService.Addr)
	if err != nil {
		logger.Fatalf("Cannot start the account service: %v", err)
	}

	saleService, _, err = sale.NewClient(fmt.Sprintf("127.0.0.1:%d", conf.Service.GrpcPort))
	if err != nil {
		logger.Fatalf("Cannot start the sale service: %v", err)
	}

	if err := run(); err != nil {
		logger.Fatal(err)
	}
}

// HealthcheckHandler handle the healthcheck request
func HealthcheckHandler(res http.ResponseWriter, req *http.Request) {
	res.WriteHeader(http.StatusOK)
	res.Header().Set("Content-Type", "application/json")
	msg, _ := json.Marshal(map[string]string{"status": "ok", "version": version})
	res.Write([]byte(msg))
	return
}
