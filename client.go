package sale

import (
	"fmt"

	salePb "bitbucket.org/canopei/sale/protobuf"
	"google.golang.org/grpc"
)

// NewClient returns a gRPC client for interacting with the sale service.
func NewClient(addr string) (salePb.SaleServiceClient, func() error, error) {
	conn, err := grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		return nil, nil, fmt.Errorf("did not connect: %v", err)
	}

	return salePb.NewSaleServiceClient(conn), conn.Close, nil
}
